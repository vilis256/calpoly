import processing.core.PImage;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Miner implements AnimatedEntity, ActiveEntity {



    private String id;
    private Point position;
    private List<PImage> images;
    private int imageIndex;
    private int resourceLimit;
    private int resourceCount;
    private int actionPeriod;
    private int animationPeriod;

    public Miner(String id, Point position, List<PImage> images, int resourceLimit, int resourceCount, int actionPeriod, int animationPeriod) {
        this.id = id;
        this.position = position;
        this.images = images;
        this.resourceLimit = resourceLimit;
        this.resourceCount = resourceCount;
        this.actionPeriod = actionPeriod;
        this.animationPeriod = animationPeriod;
    }

    private Point nextPosition(WorldModel world,
                              Point destPos) {
        int horiz = Integer.signum(destPos.x() - this.position.x());
        Point newPos = new Point(this.position.x() + horiz,
                this.position.y());

        if (horiz == 0 || newPos.isOccupied(world)) {
            int vert = Integer.signum(destPos.y() - this.position.y());
            newPos = new Point(this.position.x(),
                    this.position.y() + vert);

            if (vert == 0 || newPos.isOccupied(world)) {
                newPos = this.position;
            }
        }

        return newPos;
    }

    public void executeActivity(WorldModel world, ImageStore imageStore, EventScheduler scheduler) {
        if (resourceCount == resourceLimit) {
            Optional<Entity> fullTarget = this.position.findNearest(world,
                    Blacksmith.class);

            if (fullTarget.isPresent() &&
                    this.moveToFull(world, fullTarget.get(), scheduler)) {
                this.transformFull(world, scheduler, imageStore);
            } else {
                scheduler.scheduleEvent(this,
                        new Activity(this, world, imageStore),
                        this.actionPeriod);
            }
        } else {
            Optional<Entity> notFullTarget = this.position.findNearest(world,
                    Ore.class);

            if (!notFullTarget.isPresent() ||
                    !this.moveToNotFull(world, notFullTarget.get(), scheduler) ||
                    !this.transformNotFull(world, scheduler, imageStore)) {
                scheduler.scheduleEvent(this,
                        new Activity(this, world, imageStore),
                        this.actionPeriod);
            }
        }
    }
    private boolean moveToNotFull(WorldModel world,
                                 Entity target, EventScheduler scheduler)
    {
        if (this.position.adjacent(target.getPosition()))
        {
            this.resourceCount += 1;
            world.removeEntity(target);
            scheduler.unscheduleAllEvents(target);

            return true;
        }
        else
        {
            Point nextPos = this.nextPosition(world, target.getPosition());

            if (!this.position.equals(nextPos))
            {
                Optional<Entity> occupant = world.getOccupant(nextPos);
                if (occupant.isPresent())
                {
                    scheduler.unscheduleAllEvents(occupant.get());
                }

                world.moveEntity(this, nextPos);
            }
            return false;
        }
    }
    private boolean moveToFull(WorldModel world,
                              Entity target, EventScheduler scheduler)
    {
        if (this.position.adjacent(target.getPosition()))
        {
            return true;
        }
        else
        {
            Point nextPos = this.nextPosition(world, target.getPosition());

            if (!this.position.equals(nextPos))
            {
                Optional<Entity> occupant = world.getOccupant(nextPos);
                if (occupant.isPresent())
                {
                    scheduler.unscheduleAllEvents(occupant.get());
                }

                world.moveEntity(this, nextPos);
            }
            return false;
        }
    }
    private boolean transformNotFull(WorldModel world,
                                    EventScheduler scheduler, ImageStore imageStore)
    {
        if (this.resourceCount >= this.resourceLimit)
        {
            Entity miner = new Miner(this.id, this.position,
                    this.images,this.resourceLimit,this.resourceLimit,
                    this.actionPeriod, this.animationPeriod);

            world.removeEntity(this);
            scheduler.unscheduleAllEvents(this);

            world.addEntity(miner);
            miner.scheduleAction(scheduler, world, imageStore);

            return true;
        }

        return false;
    }
    private void transformFull(WorldModel world,
                              EventScheduler scheduler, ImageStore imageStore) {
        Entity miner = new Miner(this.id, this.position,this.images,this.resourceLimit,0,
                this.actionPeriod, this.animationPeriod);

        world.removeEntity(this);
        scheduler.unscheduleAllEvents(this);

        world.addEntity(miner);
        miner.scheduleAction(scheduler, world, imageStore);
    }
    public void nextImage() {
        this.imageIndex = (this.imageIndex + 1) % this.images.size();
    }

    public void scheduleAction(EventScheduler scheduler,
                                WorldModel world, ImageStore imageStore) {

        scheduler.scheduleEvent(this,
                new Activity(this, world, imageStore),
                this.actionPeriod);
             scheduler.scheduleEvent(this,
        new Animation(this,world,imageStore,0), getAnimationPeriod());
    }

    public int getAnimationPeriod() {
        return animationPeriod;
    }
    public PImage getCurrentImage()
    {
        return images.get(imageIndex);
    }
    public String getId() {
        return id;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    @Override
    public List<PImage> getImages() {
        return images;
    }




}
