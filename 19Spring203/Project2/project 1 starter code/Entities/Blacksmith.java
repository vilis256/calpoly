import processing.core.PImage;

import java.util.List;

public class Blacksmith implements Entity {
    private String id;
    private Point position;
    private List<PImage> images;
    private int imageIndex;
    private int animationPeriod=0;



    public Blacksmith(String id, Point position, List<PImage> images) {
        this.id = id;
        this.position = position;
        this.images = images;
    }

    public void nextImage()
    {
        this.imageIndex = (this.imageIndex + 1) % this.images.size();
    }
    public PImage getCurrentImage()
    {
        return images.get(imageIndex);
    }

    public void executeActivity(WorldModel world, ImageStore imageStore, EventScheduler scheduler) {}
    public void scheduleAction(EventScheduler scheduler,
                               WorldModel world, ImageStore imageStore) {}

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    @Override
    public List<PImage> getImages() {
        return images;
    }
    @Override
    public int getAnimationPeriod() {
        return animationPeriod;
    }

}
