import org.junit.Test;

import static org.junit.Assert.*;

public class PointTest {
    public static final double DELTA = 0.00001;

    @Test
    public void testGetX()
    {
        assertEquals(1.0, new Point(1.0, 2.0).getX(), DELTA);
    }

    @Test
    public void getY() {
    }

    @Test
    public void getRadius() {
    }

    @Test
    public void getAngle() {
    }

    @Test
    public void rotate() {
    }
}