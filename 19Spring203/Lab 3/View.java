public class View {
    private String sessionID, productID;
    private int price;

    public String getProduct() {
        return productID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public int getPrice() {
        return price;
    }

    public View(String sessionID,String productID, int price) {
        this.productID = productID;
        this.sessionID = sessionID;
        this.price = price;
    }
}
