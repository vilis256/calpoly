public class Buy {
    private String sessionID, productID;
    private int price,quantity;

    public String getSessionID() {
        return sessionID;
    }

    public String getProductID() {
        return productID;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public Buy(String sessionID, String productID, int price, int quantity) {
        this.sessionID = sessionID;
        this.productID = productID;
        this.price = price;
        this.quantity = quantity;
    }
}
