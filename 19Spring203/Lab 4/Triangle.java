import java.awt.Color;
import java.awt.Point;
import java.util.*;


public class Triangle implements Shape {
    private Point a,b,c;
    private Color color;

    public Triangle(Point a1, Point b1, Point c1, Color col) {
        a=a1;
        b=b1;
        c=c1;
        this.color = col;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color c) {
        color = c;
    }

    @Override
    public double getArea() {
        return (a.x*(b.y-c.y)+b.x*(c.y-a.y)+c.x*(a.y-b.y))/2;
    }

    @Override
    public double getPerimeter() {
        return a.distance(b)+b.distance(c)+c.distance(a);
    }

    @Override
    public void translate(Point p) {
        a.translate(p.x,p.y);
        b.translate(p.x,p.y);
        c.translate(p.x,p.y);
    }

    public Point getVertexA() {
        return a;
    }

    public Point getVertexB() {
        return b;
    }

    public Point getVertexC() {
        return c;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (a != null ? !a.equals(triangle.a) : triangle.a != null) return false;
        if (b != null ? !b.equals(triangle.b) : triangle.b != null) return false;
        if (c != null ? !c.equals(triangle.c) : triangle.c != null) return false;
        return color != null ? color.equals(triangle.color) : triangle.color == null;

    }
}
