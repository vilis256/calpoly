import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class WorkSpace {
    private List<Shape> shapes = new ArrayList<>();
    public void add(Shape s){shapes.add(s);}
    public Shape get(int i){return shapes.get(i);}
    public int size(){return shapes.size();}
    public List<Circle> getCircles(){
        List<Circle> ret = new ArrayList<>();
        for(Shape s:shapes){
            if(s instanceof Circle) ret.add((Circle)s);
        }
        return ret;
    }
    public List<Rectangle> getRectangles(){
        List<Rectangle> ret = new ArrayList<>();
        for(Shape s:shapes){
            if(s instanceof Rectangle) ret.add((Rectangle)s);
        }
        return ret;
    }
    public List<Triangle> getTriangles(){
        List<Triangle> ret = new ArrayList<>();
        for(Shape s:shapes){
            if(s instanceof Triangle) ret.add((Triangle)s);
        }
        return ret;
    }
    public List<Shape> getShapesByColor(Color c){
        List<Shape> ret = new ArrayList<>();
        for(Shape s:shapes){
            if(s.getColor().equals(c)) ret.add(s);
        }
        return ret;
    }
    public double getAreaOfAllShapes(){
        double sum=0;
        for(Shape s:shapes){
            sum+=s.getArea();
        }
        return sum;
    }
    public double getPerimeterOfAllShapes(){
        double sum=0;
        for(Shape s:shapes){
            sum+=s.getPerimeter();
        }
        return sum;
    }

}
