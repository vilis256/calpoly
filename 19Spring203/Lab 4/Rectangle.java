import java.awt.*;

public class Rectangle implements Shape {
    private Point topLeft;
    private double width, height;
    private Color color;

    public Rectangle(double width, double height, Point topLeft, Color c) {
        this.topLeft = topLeft;
        this.width = width;
        this.height = height;
        this.color = c;
    }


    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color c) {
        color = c;
    }

    @Override
    public double getArea() {
        return width*height;
    }

    @Override
    public double getPerimeter() {
        return 2*(width+height);
    }

    @Override
    public void translate(Point p) {
        topLeft.translate(p.x,p.y);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.width, width) != 0) return false;
        if (Double.compare(rectangle.height, height) != 0) return false;
        if (topLeft != null ? !topLeft.equals(rectangle.topLeft) : rectangle.topLeft != null) return false;
        return color != null ? color.equals(rectangle.color) : rectangle.color == null;

    }

}
