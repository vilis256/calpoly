import processing.core.PImage;

import java.util.List;
import java.util.Optional;

public abstract class ActimatedEntity extends AnimatedEntity implements Active {
    private int actionPeriod;

    public ActimatedEntity(String id, Point position, int animationPeriod, List<PImage> images, int imageIndex, int actionPeriod) {
        super(id, position, animationPeriod, images, imageIndex);
        this.actionPeriod = actionPeriod;
    }

    public int getActionPeriod(){
        return actionPeriod;
    }
    protected Point nextPosition(WorldModel world,
                               Point destPos)
    {
        int horiz = Integer.signum(destPos.x() - super.getPosition().x());
        Point newPos = new Point(super.getPosition().x() + horiz,
                super.getPosition().y());

        Optional<Entity> occupant = world.getOccupant(newPos);

        if (horiz == 0 ||
                (occupant.isPresent() && !(occupant.get().getClass() == this.getClass())))
        {
            int vert = Integer.signum(destPos.y() - super.getPosition().y());
            newPos = new Point(super.getPosition().x(), super.getPosition().y() + vert);
            occupant = world.getOccupant(newPos);

            if (vert == 0 ||
                    (occupant.isPresent() && !(occupant.get().getClass() == this.getClass())))
            {
                newPos = super.getPosition();
            }
        }

        return newPos;
    }
    protected boolean moveNextPos(WorldModel world, Entity target, EventScheduler scheduler) {
        Point nextPos = this.nextPosition(world, target.getPosition());

        if (!super.getPosition().equals(nextPos)) {
            Optional<Entity> occupant = world.getOccupant(nextPos);
            if (occupant.isPresent()) {
                scheduler.unscheduleAllEvents(occupant.get());
            }

            world.moveEntity(this, nextPos);
        }
        return false;
    }
    public void scheduleAction(EventScheduler scheduler,
                               WorldModel world, ImageStore imageStore){
        scheduler.scheduleEvent(this,
                new Activity(this, world, imageStore),
                getActionPeriod());
        scheduler.scheduleEvent(this,
                new Animation(this,world,imageStore,0), getAnimationPeriod());
    }
}
