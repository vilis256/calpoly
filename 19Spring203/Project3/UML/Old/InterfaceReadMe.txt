I put the findNearest(world,type) function in Point
as it makes sense to allow the functionality
of area controlled events which aren't reliant
on an entity being present, or that entity's idea of 
what should be found. 