public abstract class BinaryExpression implements Expression {

    private final Expression lft;
    private final Expression rht;
    private final String op;

    public BinaryExpression(Expression lft, Expression rht, String op) {
        this.lft = lft;
        this.rht = rht;
        this.op=op;
    }
    public String toString()
    {
        return "(" + lft + op + rht + ")";
    }

    public double evaluate(final Bindings bindings)
    {
        return _applyOperators(lft.evaluate(bindings), rht.evaluate(bindings));
    }

    protected abstract double _applyOperators(Double lft, double rht);
}
