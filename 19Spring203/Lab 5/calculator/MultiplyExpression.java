class MultiplyExpression extends BinaryExpression
   implements Expression
{

   public MultiplyExpression(final Expression lft, final Expression rht)
   {
      super(lft,rht," * ");
   }
   @Override
   protected double _applyOperators(Double lft, double rht) {
      return lft*rht;
   }
//   public double evaluate(final Bindings bindings)
//   {
//      return lft.evaluate(bindings) * rht.evaluate(bindings);
//   }
}

