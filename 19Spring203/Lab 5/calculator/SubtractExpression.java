class SubtractExpression extends BinaryExpression implements Expression
{


   public SubtractExpression(final Expression lft, final Expression rht)
   {
      super(lft,rht," - ");
   }

   @Override
   protected double _applyOperators(Double lft, double rht) {
      return lft-rht;
   }
}

