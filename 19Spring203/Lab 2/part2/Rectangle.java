public class Rectangle {
    private Point topL, botR;

    public Rectangle(Point topL, Point botR) {
        this.topL = topL;
        this.botR = botR;
    }

    public Point getTopLeft() {
        return topL;
    }

    public Point getBottomRight() {
        return botR;
    }

    public double perimeter(){
        Point p0 = this.getBottomRight(), p1 = this.getTopLeft();
        Double x0 = p0.getX(), x1 = p1.getX(), y0=p0.getY(), y1=p1.getY();
        return 2*(Math.abs(x0-x1) + Math.abs(y0 - y1));
    }
}
