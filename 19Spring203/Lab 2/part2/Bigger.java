public class Bigger {
    public static double whichIsBigger(Circle c, Rectangle r, Polygon p){
        double cp = c.perimeter(), rp = r.perimeter(), pp = p.perimeter();
        if(cp >= rp){
            if(cp >= pp){
                return cp;
            }else{
                return pp;
            }
        } else if (rp >= pp) {
            return rp;
        }
         return pp;

    }
}
