import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Polygon {
    private List<Point> Points = new ArrayList<>();

    public Polygon(List<Point> points) {
        Points = points;
    }

    public List<Point> getPoints() {
        return Points;
    }

    public double perimeter(){
        double sum = 0;
        Point p0 = this.getPoints().get(0);
        Point pp = p0;
        for(int i=1; i<this.getPoints().size(); i++){
            if(i>1)
                p0 = this.getPoints().get(i-1);
            Point p1 = this.getPoints().get(i);
            Double x0 = p0.getX(), x1 = p1.getX(), y0=p0.getY(), y1=p1.getY();

            sum += Math.sqrt(Math.abs(x0-x1)*Math.abs(x0-x1) + Math.abs(y0 - y1)*Math.abs(y0 - y1));

        }
        Point p1 = this.getPoints().get(this.getPoints().size()-1);
        sum += Math.sqrt(Math.abs(pp.getX()-p1.getX())*Math.abs(pp.getX()-p1.getX()) + Math.abs(pp.getY() - p1.getY())*Math.abs(pp.getY() - p1.getY()));
        return sum;
    }
}
