import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.concurrent.Delayed;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.w3c.dom.css.Rect;

public class PartOneTestCases
{
   public static final double DELTA = 0.00001;

   @Test
   public void testCircleImplSpecifics()
      throws NoSuchMethodException
   {
      final List<String> expectedMethodNames = Arrays.asList(
         "getCenter", "getRadius");

      final List<Class> expectedMethodReturns = Arrays.asList(
         Point.class, double.class);

      final List<Class[]> expectedMethodParameters = Arrays.asList(
         new Class[0], new Class[0]);

      verifyImplSpecifics(Circle.class, expectedMethodNames,
         expectedMethodReturns, expectedMethodParameters);



   }

   @Test
   public void testRectangleImplSpecifics()
      throws NoSuchMethodException
   {
      final List<String> expectedMethodNames = Arrays.asList(
         "getTopLeft", "getBottomRight");

      final List<Class> expectedMethodReturns = Arrays.asList(
         Point.class, Point.class);

      final List<Class[]> expectedMethodParameters = Arrays.asList(
         new Class[0], new Class[0]);

      verifyImplSpecifics(Rectangle.class, expectedMethodNames,
         expectedMethodReturns, expectedMethodParameters);
   }

   @Test
   public void testPolygonImplSpecifics()
      throws NoSuchMethodException
   {
      final List<String> expectedMethodNames = Arrays.asList(
         "getPoints");

      final List<Class> expectedMethodReturns = Arrays.asList(
         List.class);

      final List<Class[]> expectedMethodParameters = Arrays.asList(
         new Class[][] {new Class[0]});

      verifyImplSpecifics(Polygon.class, expectedMethodNames,
         expectedMethodReturns, expectedMethodParameters);
   }

   @Test
   public void testUtilImplSpecifics()
      throws NoSuchMethodException
   {
      final List<String> expectedMethodNames = Arrays.asList(
         "perimeter", "perimeter", "perimeter");

      final List<Class> expectedMethodReturns = Arrays.asList(
         double.class, double.class, double.class);

      final List<Class[]> expectedMethodParameters = Arrays.asList(
         new Class[] {Circle.class},
         new Class[] {Polygon.class},
         new Class[] {Rectangle.class});

      verifyImplSpecifics(Util.class, expectedMethodNames,
         expectedMethodReturns, expectedMethodParameters);

   }

   @Test
   public void testCirclePerimeter(){
      Circle k = new Circle(new Point(1,1), Math.PI);
      assertEquals(2*Math.PI*Math.PI, Util.perimeter(k), DELTA);
   }
   @Test
   public void testRectPerimeter(){
      Rectangle r = new Rectangle(new Point(3,0), new Point(0,2));
      assertEquals(10, Util.perimeter(r), DELTA);
   }
   @Test
   public void testRectPerimeter1(){
      Rectangle r = new Rectangle(new Point(-Math.PI,3), new Point(5.45,-3.876));
      assertEquals(30.935185307179584, Util.perimeter(r), DELTA);
   }

   @Test
   public void testPolyPerimeter(){
      ArrayList<Point> points = new ArrayList<>();
      points.add(new Point(0,0));
      points.add(new Point(3,0));
      points.add(new Point(0,4));
      Polygon p = new Polygon(points);

      assertEquals(12,Util.perimeter(p),DELTA);
   }
   @Test
   public void testPolyPerimeter1(){
      ArrayList<Point> points = new ArrayList<>();
      points.add(new Point(Math.E,Math.PI));
      points.add(new Point(-Math.PI * Math.E,-2*Math.PI));
      points.add(new Point(0,0));
      points.add(new Point(Math.PI*3,Math.E));
      Polygon p = new Polygon(points);

      assertEquals(41.81321784538786,Util.perimeter(p),DELTA);
   }

   @Test
   public void testBigger(){
      Circle k =  new Circle(new Point(1,1),2);
      Rectangle r = new Rectangle(new Point(-1,2), new Point(1,-1.6));
      ArrayList<Point> ps = new ArrayList<>();
      ps.add(new Point(0,0));
      ps.add(new Point(3,1));
      ps.add(new Point(1,4));
      ps.add(new Point(-1,4));
      Polygon p = new Polygon(ps);
      System.out.println(Util.perimeter(k) + " " + Util.perimeter(r) + " " + Util.perimeter(p));
      assertEquals(12.890934561250031, Bigger.whichIsBigger(k,r,p), DELTA);
   }

   private static void verifyImplSpecifics(
      final Class<?> clazz,
      final List<String> expectedMethodNames,
      final List<Class> expectedMethodReturns,
      final List<Class[]> expectedMethodParameters)
      throws NoSuchMethodException
   {
      assertEquals("Unexpected number of public fields",
         0, clazz.getFields().length);

      final List<Method> publicMethods = Arrays.stream(
         clazz.getDeclaredMethods())
            .filter(m -> Modifier.isPublic(m.getModifiers()))
            .collect(Collectors.toList());

      assertEquals("Unexpected number of public methods",
         expectedMethodNames.size(), publicMethods.size());

      assertTrue("Invalid test configuration",
         expectedMethodNames.size() == expectedMethodReturns.size());
      assertTrue("Invalid test configuration",
         expectedMethodNames.size() == expectedMethodParameters.size());

      for (int i = 0; i < expectedMethodNames.size(); i++)
      {
         Method method = clazz.getDeclaredMethod(expectedMethodNames.get(i),
            expectedMethodParameters.get(i));
         assertEquals(expectedMethodReturns.get(i), method.getReturnType());
      }
   }
}
