
public class Point{
  private double x;
  private double y;
  public Point(){}
  public Point(double x1, double y1){
    x=x1;
    y=y1;
  }
  public double getX(){return x;}
  public double getY(){return y;}
  public double getRadius(){return Math.sqrt(x*x+y*y);}
  public double getAngle(){return Math.atan(x/y);}
  public Point rotate(){return new Point(-y,x);}

}
