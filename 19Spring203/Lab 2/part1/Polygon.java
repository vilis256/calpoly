import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Polygon {
    private List<Point> Points = new ArrayList<>();

    public Polygon(List<Point> points) {
        Points = points;
    }

    public List<Point> getPoints() {
        return Points;
    }
}
