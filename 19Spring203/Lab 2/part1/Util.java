public class Util {

    public static double perimeter(Circle c){
        return Math.PI * 2 * c.getRadius();
    }
    public static double perimeter(Rectangle r){
        Point p0 = r.getBottomRight(), p1 = r.getTopLeft();
        Double x0 = p0.getX(), x1 = p1.getX(), y0=p0.getY(), y1=p1.getY();
        return 2*(Math.abs(x0-x1) + Math.abs(y0 - y1));
    }
    public static double perimeter(Polygon p){
        double sum = 0;
        Point p0 = p.getPoints().get(0);
        Point pp = p0;
        for(int i=1; i<p.getPoints().size(); i++){
            if(i>1)
                p0 = p.getPoints().get(i-1);
            Point p1 = p.getPoints().get(i);
            Double x0 = p0.getX(), x1 = p1.getX(), y0=p0.getY(), y1=p1.getY();

            sum += Math.sqrt(Math.abs(x0-x1)*Math.abs(x0-x1) + Math.abs(y0 - y1)*Math.abs(y0 - y1));

        }
        Point p1 = p.getPoints().get(p.getPoints().size()-1);
        sum += Math.sqrt(Math.abs(pp.getX()-p1.getX())*Math.abs(pp.getX()-p1.getX()) + Math.abs(pp.getY() - p1.getY())*Math.abs(pp.getY() - p1.getY()));
        return sum;
    }
}
