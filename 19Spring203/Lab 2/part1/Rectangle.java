public class Rectangle {
    private Point topL, botR;

    public Rectangle(Point topL, Point botR) {
        this.topL = topL;
        this.botR = botR;
    }

    public Point getTopLeft() {
        return topL;
    }

    public Point getBottomRight() {
        return botR;
    }
}
