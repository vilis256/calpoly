public class Bigger {
    public static double whichIsBigger(Circle c, Rectangle r, Polygon p){
        double cp = Util.perimeter(c), rp = Util.perimeter(r), pp = Util.perimeter(p);
        if(cp >= rp){
            if(cp >= pp){
                return cp;
            }else{
                return pp;
            }
        } else if (rp >= pp) {
            return rp;
        }
         return pp;

    }
}
