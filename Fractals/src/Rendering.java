import processing.core.PApplet;
import processing.core.PImage;

public class Rendering extends PApplet {

    private Viewport mainView,view,mother;
    private Point start=new Point(0,0),end=new Point(0,0),mouse=new Point(0,0);
    private boolean newPress=true,mothered=false;
    private Mandelbrot mandy;
    private PImage image;
    private double res=3;

    public static void main(String[] args) {
        PApplet.main("Rendering", args);

    }
    @Override
    public void settings() {
//        fullScreen();
        size(1920,1920);
    }
    public void setup(){
        background(0);
        mainView = new Viewport(width,height,new Point(width/2,height/2),this);
        mother=new Viewport(mainView);
        view=new Viewport(mainView);

        mainView.setMother(mainView);
        mother.setMother(mainView);
        view.setMother(mother);
        mandy = new Mandelbrot(this);
        image=mandy.render(view,mainView,res);
    }
    public void draw(){
        mouse.update(mouseX,mouseY);
        if(mousePressed){
            mandy.display();
            if(newPress){
                start.update(view.putIn(mouse));
                newPress=false;
            }else {
                end.update(view.putIn(mouse));
                colorMode(RGB,255);
                stroke(255);
                fill(0,255,0,50);
                view.takeOut(start).rect(mouse,this);
            }
        }

    }
    public void mousePressed(){

    }
    public void mouseClicked(){

    }
    public void mouseReleased(){
        newPress=true;
        /*if(!mothered) {
            mother.resize(start.xDist(end),start.yDist(end));
            mother.moveTo(start.center(end));
            mothered=true;
        }*/
        mother=view;
        view = view.devineBirth(start,end);
        view.setMother(mainView);
        image = mandy.render(view,mainView,res);
    }
    public void keyPressed(){
        switch(key){
            case('a'):
                view.scale(0.8);
                image = mandy.render(view,mainView,res);
                break;
            case('q'):
                view.scale(1.2);
                image = mandy.render(view,mainView,res);
                break;
            case('u'):
                view=mother;
                image = mandy.render(view,mainView,res);
                break;
            case('`'):
                view=mainView;
                mother=mainView;
                image = mandy.render(view,mainView,res);
                break;
            default:
                break;
        }
    }
}
