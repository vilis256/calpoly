import processing.core.PApplet;
import processing.core.PImage;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Mandelbrot implements Fractal {
    private Point center;
    private PApplet parent;
    private double hue = 1, sat = 1, bri = 1;
    private double maxIter = 200, scl = 1, wScale, hScale;
    private static final DateFormat dateFormat = new SimpleDateFormat("EEE_MMM_d_h'h'mm'm'ss's'_a");
    private Viewport section, screen;
    private PImage output;
    private float imgW, imgH;

    private Viewport area;

    public Mandelbrot(PApplet parent) {
        this.parent = parent;
        this.screen = new Viewport(1920, 1080, new Point(0, 0), parent);
    }

    public int iterate(Point c) {
        Point z = new Point(0, 0);
        double zx1, zy1;
        for (int i = 1; i < maxIter; i++) {
            if (c.dist(z) > 5) {
                return i;
            }
            zx1 = (z.x * z.x - z.y * z.y) + c.x;
            zy1 = (2 * z.x * z.y) + c.y;
            z.update(zx1, zy1);
        }
        return -1;
    }

    public int colorPoint(Point p) {
        parent.colorMode(parent.HSB, 255);
        int iter = iterate(p);
        return parent.color((float) (hue * 113 * (parent.sin(parent.radians(iter * 2)) + 1)),
                (float) (sat * 150 * (iter / Math.abs(iter))),
                (float) (bri * 150 * (iter / Math.abs(iter))));

    }

    @Override
    public PImage render(Viewport section, Viewport screen, double res) {
        parent.println(section);
        double nXPix, nYPix, cx = imagX(section.center().x), cy = imagY(section.center().y);
        int color;
        {
        this.section = section;
        this.screen = screen;
        wScale = screen.width();
        while (wScale > 10) {
            wScale /= 10;
        }
        hScale = screen.width();
        while (hScale > 10) {
            hScale /= 10;
        }

        if(section.width()>section.height()){
            nXPix = (int)(screen.width() / res);
            nYPix = (int)(((screen.width()/section.width())*section.height())/res);
        }else{
            nYPix = (int)(screen.height() / res);
            nXPix = (int)(((screen.height()/section.height())*section.width())/res);
        }

        output = parent.createImage((int) nXPix, (int) nYPix, parent.HSB);

        imgW = (float) (nXPix * res);
        imgH = (float) (nYPix * res);
        }
        for (double i = -nXPix / 2; i < nXPix / 2; i++) {
            for (double j = -nYPix / 2; j < nYPix / 2; j++) {
                color = colorPoint(new Point(cx + (i / (nXPix / 2)) * (imagWid(section.width()) / 2),
                        cy + (j / (nYPix / 2)) * (imagHig(section.height()) / 2)));
                output.pixels[(int) (i + nXPix / 2) + (int) ((j + nYPix / 2) * nXPix)] = color;
            }
        }
        parent.image(output, (float) (screen.center().x - imgW / 2),
                (float) (screen.center().y - imgH / 2),
                imgW, imgH);
        return output;
    }

    @Override
    public void toFile(String name) {
        List<String> lines = Arrays.asList(area + "\n" +
                hue + "\n" +
                sat + "\n" +
                bri + "\n");
        Path file = Paths.get(name);
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fromFile(String file) {
        ArrayList<Double> properties = new ArrayList<>();
        try {
            Scanner input = new Scanner(new File(file));
            while (input.hasNextLine()) {
                properties.add(Double.parseDouble(input.nextLine()));
            }
            if (properties.size() == 7) {
                area = new Viewport(properties.get(0), properties.get(1),
                        new Point(properties.get(2), properties.get(3)), parent);
                hue = properties.get(4);
                sat = properties.get(5);
                bri = properties.get(6);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void display() {
        parent.image(output, (float) (screen.center().x - imgW / 2),
                (float) (screen.center().y - imgH / 2),
                imgW, imgH);
    }

    private double imagWid(double w) {
        return wScale * (w / screen.width());
    }

    private double imagHig(double h) {
        return hScale * (h / screen.height());
    }

    private double imagX(double x) {
        return (x - screen.width() / 2) * (wScale / screen.width());
    }

    private double imagY(double y) {
        return (y - screen.height() / 2) * (hScale / screen.height());
    }
/*
    private double newX(double x){
        return ((rendPX+((x-viewWidth/2)/scl))-viewWidth/2)*(wScale/viewWidth);
    }
    private double newY(double y){
        return (((rendPY+(y-viewHeight/2)/scl))-viewHeight/2)*(hScale/viewHeight);
    }

    private double realX(double x){ return ((viewWidth/2)+(x/(wScale/viewWidth))) - scl*(rendPX-viewWidth/2);}
    private double realY(double y){ return ((viewHeight/2)+(y/(hScale/viewHeight))) - scl*(rendPY-viewHeight/2);}
    private double transX(double x) {
        return (rendPX+((x-viewWidth/2)/scl));
    }
    private double transY(double y) {
        return ((rendPY+(y-viewHeight/2)/scl));
    }*/
}
