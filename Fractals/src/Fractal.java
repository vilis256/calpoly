import processing.core.PImage;

public interface Fractal {
    PImage render(Viewport section, Viewport screen, double res);
    void toFile(String name);
    void fromFile(String name);
}